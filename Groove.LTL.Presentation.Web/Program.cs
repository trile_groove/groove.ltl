﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Program Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Presentation.Web
{
    using System.IO;

    using Microsoft.AspNetCore.Hosting;

    /// <summary>
    /// Program Class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Application bootstrap function with specified arguments
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();

            host.Run();
        }
    }
}
