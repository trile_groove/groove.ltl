﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Groove.LTL.Application.Customer.Interfaces;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Notification;
using Groove.LTL.Domain.Customer.Models;
using Groove.LTL.Application.Customer.ViewModels;

namespace Groove.LTL.Presentation.Web.Controllers
{
    /// <summary>
    /// Home Controller Class
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    public class HomeController : BaseController
    {
        private readonly ICustomerService _customerAppService;

        public HomeController(ICustomerService customerAppService, IDomainNotificationHandler<DomainNotification> notifications) : base(notifications)
        {
            _customerAppService = customerAppService;
        }

        /// <summary>
        /// Returns controller's index view
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Shows About View
        /// </summary>
        /// <returns></returns>
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        /// <summary>
        /// Shows Contact View.
        /// </summary>
        /// <returns></returns>
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult AddCustomer()
        {
            CustomerViewModel test = new CustomerViewModel()
            {
                CompanyName = "testCore",
                ContactName = "testCoreName",
                Email = "test@email.com",
            };

            _customerAppService.Create(test);

            return View();
        }

        /// <summary>
        /// Shows Error View.
        /// </summary>
        /// <returns></returns>
        public IActionResult Error()
        {
            return View();
        }
    }
}
