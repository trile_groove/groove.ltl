﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FactorViewModel.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Factor ViewModel Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Identity.Models.ManageViewModels
{
    /// <summary>
    /// Factor ViewModel Class
    /// </summary>
    public class FactorViewModel
    {
        /// <summary>
        /// Gets or sets the purpose.
        /// </summary>
        public string Purpose { get; set; }
    }
}
