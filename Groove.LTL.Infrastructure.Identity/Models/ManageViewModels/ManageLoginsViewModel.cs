﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ManageLoginsViewModel.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Manage Logins ViewModel Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Identity.Models.ManageViewModels
{
    using System.Collections.Generic;

    using Microsoft.AspNetCore.Http.Authentication;
    using Microsoft.AspNetCore.Identity;

    /// <summary>
    /// Manage Logins ViewModel Class
    /// </summary>
    public class ManageLoginsViewModel
    {
        /// <summary>
        /// Gets or sets the current logins.
        /// </summary>
        public IList<UserLoginInfo> CurrentLogins { get; set; }

        /// <summary>
        /// Gets or sets the other logins.
        /// </summary>
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }
}
