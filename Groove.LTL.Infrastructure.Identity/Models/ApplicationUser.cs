﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationUser.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Adds profile data for application users by adding properties to the Application User class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Identity.Models
{
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

    /// <summary>
    /// Adds profile data for application users by adding properties to the Application User class
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUser" />
    public class ApplicationUser : IdentityUser
    {
    }
}
