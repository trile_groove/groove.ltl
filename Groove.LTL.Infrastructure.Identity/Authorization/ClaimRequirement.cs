﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClaimRequirement.cs" ompany="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Claim Requirement Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Identity.Authorization
{
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Claim Requirement Class
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Authorization.IAuthorizationRequirement" />
    public class ClaimRequirement : IAuthorizationRequirement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClaimRequirement"/> class.
        /// </summary>
        /// <param name="claimName">Name of the claim.</param>
        /// <param name="claimValue">The claim value.</param>
        public ClaimRequirement(string claimName, string claimValue)
        {
            this.ClaimName = claimName;
            this.ClaimValue = claimValue;
        }

        /// <summary>
        /// Gets or sets the claim name.
        /// </summary>
        public string ClaimName { get; set; }

        /// <summary>
        /// Gets or sets the claim value.
        /// </summary>
        public string ClaimValue { get; set; }
    }
}
