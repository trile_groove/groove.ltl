﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISmsSender.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   SMS Sender Interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Identity.Services
{
    using System.Threading.Tasks;

    /// <summary>
    /// SMS Sender Interface
    /// </summary>
    public interface ISmsSender
    {
        /// <summary>
        /// Sends the SMS asynchronous.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="message">The message.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        Task SendSmsAsync(string number, string message);
    }
}
