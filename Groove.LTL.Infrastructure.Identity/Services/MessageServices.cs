﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageServices.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   This class is used by the application to send Email and SMS
//   when you turn on two-factor authentication in ASP.NET Identity.
//   For more details see this link <see cref="https://go.microsoft.com/fwlink/?LinkID=532713" />
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Identity.Services
{
    using System.Threading.Tasks;

    /// <summary>
    /// This class is used by the application to send Email and SMS
    /// when you turn on two-factor authentication in ASP.NET Identity.
    /// For more details see this link <see cref="https://go.microsoft.com/fwlink/?LinkID=532713"/> 
    /// </summary>
    /// <seealso cref="Groove.LTL.Infrastructure.Identity.Services.IEmailSender" />
    /// <seealso cref="Groove.LTL.Infrastructure.Identity.Services.ISmsSender" />
    public class MessageServices : IEmailSender, ISmsSender
    {
        /// <summary>
        /// Sends the email asynchronous.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="message">The message.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }

        /// <summary>
        /// Sends the SMS asynchronous.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="message">The message.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
