﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailSender.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Email Sender Interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Identity.Services
{
    using System.Threading.Tasks;

    /// <summary>
    /// Email Sender Interface
    /// </summary>
    public interface IEmailSender
    {
        /// <summary>
        /// Sends the email asynchronous.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="message">The message.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        Task SendEmailAsync(string email, string subject, string message);
    }
}
