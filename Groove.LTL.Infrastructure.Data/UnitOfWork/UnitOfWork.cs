﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitOfWork.cs" ompany="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Unit of Work Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Data.UnitOfWork
{
    using Groove.LTL.Domain.Core.Commands;
    using Groove.LTL.Domain.Core.Interfaces;
    using Groove.LTL.Infrastructure.Data.Contexts;

    /// <summary>
    /// Unit of Work Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Interfaces.IUnitOfWork" />
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// The database context.
        /// </summary>
        private readonly LtlContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UnitOfWork(LtlContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <returns>The <see cref="CommandResponse"/>.</returns>
        public CommandResponse Commit()
        {
            var rowsAffected = this.context.SaveChanges();

            return new CommandResponse(rowsAffected > 0);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}
