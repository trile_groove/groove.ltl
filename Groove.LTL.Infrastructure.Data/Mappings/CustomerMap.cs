﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerMap.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Customer Map Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Data.Mappings
{
    using Groove.LTL.Domain.Customer.Models;
    using Groove.LTL.Infrastructure.Data.Extensions;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// Customer Map Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Infrastructure.Data.Extensions.EntityTypeConfiguration{CustomerModel}" />
    public class CustomerMap : EntityTypeConfiguration<CustomerModel>
    {
        /// <summary>
        /// Maps the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        public override void Map(EntityTypeBuilder<CustomerModel> builder)
        {
            builder.Property(c => c.Id)
                .HasColumnName("Id");

            builder.Property(c => c.CompanyName)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.ContactName)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.Email)
                .HasColumnType("varchar(100)")
                .HasMaxLength(11)
                .IsRequired();
        }
    }
}
