﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Repository.cs" ompany="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Repository Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using Groove.LTL.Domain.Core.Interfaces;
    using Groove.LTL.Infrastructure.Data.Contexts;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Repository Class
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <seealso cref="Groove.LTL.Domain.Core.Interfaces.IRepository{TEntity}" />
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{TEntity}"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public Repository(LtlContext context)
        {
            this.DbContext = context;
            this.DbSet = this.DbContext.Set<TEntity>();
        }

        /// <summary>
        /// Gets or sets the database context.
        /// </summary>
        protected LtlContext DbContext { get; set; }

        /// <summary>
        /// Gets or sets the database set.
        /// </summary>
        protected DbSet<TEntity> DbSet { get; set; }

        /// <summary>
        /// Adds the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public virtual void Add(TEntity obj)
        {
            this.DbSet.Add(obj);
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The <see cref="TEntity"/>.</returns>
        public virtual TEntity GetById(int id)
        {
            return this.DbSet.Find(id);
        }

        /// <summary>
        /// Gets all specified entities.
        /// </summary>
        /// <returns>Enumerable list of <see cref="TEntity"/>.</returns>
        public virtual IEnumerable<TEntity> GetAll()
        {
            return this.DbSet.ToList();
        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The <see cref="TEntity"/> object.</param>
        public virtual void Update(TEntity obj)
        {
            this.DbSet.Update(obj);
        }

        /// <summary>
        /// Removes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public virtual void Remove(int id)
        {
            this.DbSet.Remove(this.DbSet.Find(id));
        }

        /// <summary>
        /// Finds the specified predicate.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <returns>Enumerable list of <see cref="TEntity"/></returns>
        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return this.DbSet.AsNoTracking().Where(predicate);
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns>Number of changes saved</returns>
        public int SaveChanges()
        {
            return this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.DbContext.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}
