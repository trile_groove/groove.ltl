﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModelBuilderExtension.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Model Builder Extension Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Data.Extensions
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Model Builder Extension Class
    /// </summary>
    public static class ModelBuilderExtension
    {
        /// <summary>
        /// Adds the configuration.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="modelBuilder">The model builder.</param>
        /// <param name="configuration">The configuration.</param>
        public static void AddConfiguration<TEntity>(this ModelBuilder modelBuilder, EntityTypeConfiguration<TEntity> configuration) where TEntity : class
        {
            configuration.Map(modelBuilder.Entity<TEntity>());
        }
    }
}
