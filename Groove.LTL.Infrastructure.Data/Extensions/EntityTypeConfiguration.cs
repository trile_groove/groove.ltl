﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntityTypeConfiguration.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Entity Type Configuration Class Base
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Infrastructure.Data.Extensions
{
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// Entity Type Configuration Class Base
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public abstract class EntityTypeConfiguration<TEntity> where TEntity : class
    {
        /// <summary>
        /// Maps the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        public abstract void Map(EntityTypeBuilder<TEntity> builder);
    }
}
