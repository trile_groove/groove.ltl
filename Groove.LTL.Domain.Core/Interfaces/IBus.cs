﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBus.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// </copyright>
// <summary>
//   Service Bus Interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Interfaces
{
    using Groove.LTL.Domain.Core.Events;
    using Groove.LTL.Domain.Core.Commands;

    /// <summary>
    /// Service Bus Interface
    /// </summary>
    public interface IBus
    {
        /// <summary>
        /// Raises the event.
        /// </summary>
        /// <typeparam name="T">type of Event</typeparam>
        /// <param name="theEvent">The event.</param>
        void RaiseEvent<T>(T theEvent) where T : Event;

        /// <summary>
        /// Send the event.
        /// </summary>
        /// <typeparam name="T">type of Command</typeparam>
        /// <param name="theCommand">The command.</param>
        void SendCommand<T>(T theCommand) where T : Command;
    }
}
