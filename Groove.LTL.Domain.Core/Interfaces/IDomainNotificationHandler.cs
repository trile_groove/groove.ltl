﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDomainNotificationHandler.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Domain Notification Handler Interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Interfaces
{
    using System.Collections.Generic;
    using Groove.LTL.Domain.Core.Models;

    /// <summary>
    /// Domain Notification Handler Interface
    /// </summary>
    /// <typeparam name="T">type of Message</typeparam>
    /// <seealso cref="IMessageHandler{T}" />
    public interface IDomainNotificationHandler<T> : IMessageHandler<T> where T : Message
    {
        /// <summary>
        /// Determines whether this instance has notifications.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance has notifications; otherwise, <c>false</c>.
        /// </returns>
        bool HasNotifications();

        /// <summary>
        /// Gets the notifications.
        /// </summary>
        /// <returns>List of notifications</returns>
        List<T> GetNotifications();
    }
}
