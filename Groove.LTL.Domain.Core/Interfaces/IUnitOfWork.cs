﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUnitOfWork.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// </copyright>
// <summary>
//   Unit of Work Interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Interfaces
{
    using System;

    using Groove.LTL.Domain.Core.Commands;

    /// <summary>
    /// Unit of Work Interface
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <returns>The <see cref="CommandResponse"/>.</returns>
        CommandResponse Commit();
    }
}
