﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMessageHandler.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Handler Inteface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Interfaces
{
    using Groove.LTL.Domain.Core.Models;

    /// <summary>
    /// Message Handler Interface
    /// </summary>
    /// <typeparam name="T">type of Message</typeparam>
    public interface IMessageHandler<in T> where T : Message
    {
        /// <summary>
        /// Handles the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Handle(T message);
    }
}
