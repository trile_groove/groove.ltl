﻿using System;

using FluentValidation.Results;

using Groove.LTL.Domain.Core.Models;

namespace Groove.LTL.Domain.Core.Commands
{
    /// <summary>
    /// Command Class Base
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Models.Message" />
    public abstract class Command : Message
    {
        #region Public Properties
        public DateTime Timestamp { get; private set; }

        public ValidationResult ValidationResult { get; set; }
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Command"/> class.
        /// </summary>
        protected Command()
        {
            Timestamp = DateTime.Now;
        }

        /// <summary>
        /// Returns true if the command is valid.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool IsValid();
    }
}
