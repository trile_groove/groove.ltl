﻿namespace Groove.LTL.Domain.Core.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandResponse
    {
        public static CommandResponse Ok = new CommandResponse(true);
        public static CommandResponse Fail = new CommandResponse();

        public bool Success { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandResponse"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public CommandResponse(bool success = false)
        {
            Success = success;
        }
    }
}
