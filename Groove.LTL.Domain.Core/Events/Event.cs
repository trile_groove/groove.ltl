﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Event.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Event Class Base
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Events
{
    using System;
    using Groove.LTL.Domain.Core.Models;

    /// <summary>
    /// Event Class Base
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Models.Message" />
    public abstract class Event : Message
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Event"/> class.
        /// </summary>
        protected Event()
        {
            this.Timestamp = DateTime.Now;
        }

        /// <summary>
        /// Gets the timestamp.
        /// </summary>
        public DateTime Timestamp { get; private set; }
    }
}
