﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InMemoryBus.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   In-Memory Bus Sealed Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Groove.LTL.Domain.Core.Commands;

namespace Groove.LTL.Domain.Core.Bus
{
    using System;

    using Groove.LTL.Domain.Core.Events;
    using Groove.LTL.Domain.Core.Interfaces;
    using Groove.LTL.Domain.Core.Models;

    /// <summary>
    /// In-Memory Bus Sealed Class
    /// </summary>
    public sealed class InMemoryBus : IBus
    {
        /// <summary>
        /// Gets or sets the container accessor.
        /// </summary>
        public static Func<IServiceProvider> ContainerAccessor { get; set; }

        /// <summary>
        /// The container.
        /// </summary>
        private static IServiceProvider Container => ContainerAccessor();

        /// <summary>
        /// Raises the event.
        /// </summary>
        /// <param name="theEvent">The the event.</param>
        /// <typeparam name="T">type of Event</typeparam>
        public void RaiseEvent<T>(T theEvent) where T : Event
        {
            Publish(theEvent);
        }

        public void SendCommand<T>(T theCommand) where T : Command
        {
            Publish(theCommand);
        }

        /// <summary>
        /// Publishes the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <typeparam name="T">type of Message</typeparam>
        private static void Publish<T>(T message) where T : Message
        {
            if (Container == null)
            {
                return;
            }

            var obj = Container.GetService(message.MessageType.Equals("DomainNotification")
                ? typeof(IDomainNotificationHandler<T>)
                : typeof(IMessageHandler<T>));

            ((IMessageHandler<T>)obj).Handle(message);
        }

        /// <summary>
        /// Gets service.
        /// </summary>
        /// <param name="serviceType">The service type.</param>
        /// <returns>The <see cref="object"/>.</returns>
        private object GetService(Type serviceType)
        {
            return Container.GetService(serviceType);
        }

        /// <summary>
        /// Gets service.
        /// </summary>
        /// <typeparam name="T">type of Service</typeparam>
        /// <returns>The <see cref="T"/>.</returns>
        private T GetService<T>()
        {
            return (T)Container.GetService(typeof(T));
        }
    }
}
