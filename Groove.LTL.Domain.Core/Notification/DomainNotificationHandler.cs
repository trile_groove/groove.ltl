﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DomainNotificationHandler.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Domain Notification Handler Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Notification
{
    using System.Collections.Generic;
    using System.Linq;

    using Groove.LTL.Domain.Core.Interfaces;

    /// <summary>
    /// Domain Notification Handler Class
    /// </summary>
    /// <seealso cref="IDomainNotificationHandler{T}" />
    public class DomainNotificationHandler : IDomainNotificationHandler<DomainNotification>
    {
        /// <summary>
        /// The notifications list.
        /// </summary>
        private List<DomainNotification> notifications;

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainNotificationHandler"/> class.
        /// </summary>
        public DomainNotificationHandler()
        {
            this.notifications = new List<DomainNotification>();
        }

        /// <summary>
        /// Handles the domain notification message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(DomainNotification message)
        {
            this.notifications.Add(message);
        }

        /// <summary>
        /// Gets the notifications.
        /// </summary>
        /// <returns>List of <see cref="DomainNotification"/>.</returns>
        public List<DomainNotification> GetNotifications()
        {
            return this.notifications;
        }

        /// <summary>
        /// Determines whether this instance has notifications.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance has notifications; otherwise, <c>false</c>.
        /// </returns>
        public bool HasNotifications()
        {
            return this.GetNotifications().Any();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            this.notifications = new List<DomainNotification>();
        }
    }
}
