﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DomainNotification.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Domain Notification Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Notification
{
    using System;

    using Groove.LTL.Domain.Core.Events;

    /// <summary>
    /// Domain Notification Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Events.Event" />
    public class DomainNotification : Event
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DomainNotification"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public DomainNotification(string key, string value)
        {
            this.DomainNotificationId = Guid.NewGuid();
            this.Version = 1;
            this.Key = key;
            this.Value = value;
        }

        #region Properties
        /// <summary>
        /// Gets the domain notification id.
        /// </summary>
        public Guid DomainNotificationId { get; private set; }

        /// <summary>
        /// Gets the key.
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Gets the version.
        /// </summary>
        public int Version { get; private set; }
        #endregion
    }
}
