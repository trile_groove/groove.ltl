﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Message.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Message Class Base
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Models
{
    /// <summary>
    /// Message Class Base
    /// </summary>
    public abstract class Message
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        protected Message()
        {
            this.MessageType = this.GetType().Name;
        }

        /// <summary>
        /// Gets or sets the message type.
        /// </summary>
        public string MessageType { get; protected set; }

        /// <summary>
        /// Gets or sets the aggregate id.
        /// </summary>
        public int AggregateId { get; protected set; }
    }
}
