﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValueObject.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Value Object Class Base
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Core.Models
{
    /// <summary>
    /// Value Object Class Base
    /// </summary>
    /// <typeparam name="T">type of ValueObject</typeparam>
    public abstract class ValueObject<T> where T : ValueObject<T>
    {
        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">ValueObject a.</param>
        /// <param name="b">ValueObject b.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="ValueObject{T}" /> are equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator ==(ValueObject<T> a, ValueObject<T> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
            {
                return true;
            }

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
            {
                return false;
            }

            return a.Equals(b);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">ValueObject a.</param>
        /// <param name="b">ValueObject b.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="ValueObject{T}" /> are not equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator !=(ValueObject<T> a, ValueObject<T> b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var valueObject = obj as T;

            return !ReferenceEquals(valueObject, null) && this.EqualsCore(valueObject);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.GetHashCodeCore();
        }

        /// <summary>
        /// Equals to core object.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>
        ///   <c>true</c> if the specified object <see cref="T" /> is equal with current object; otherwise, <c>false</c>.
        /// </returns>
        protected abstract bool EqualsCore(T other);

        /// <summary>
        /// Gets the hash code core.
        /// </summary>
        /// <returns>Hash code core</returns>
        protected abstract int GetHashCodeCore();
    }
}
