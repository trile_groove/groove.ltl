﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace Groove.LTL.Application.Common
{
    /// <summary>
    /// Base Validation Class Base
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="FluentValidation.AbstractValidator{T}" />
    public abstract class BaseValidation<T> : AbstractValidator<T> where T : BaseViewModel
    {
    }
}
