﻿
using Groove.LTL.Domain.Core.Commands;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Notification;

namespace Groove.LTL.Application.Common
{
    /// <summary>
    /// Service Base Class
    /// </summary>
    public class BaseService
    {
        #region Private Variables
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBus _bus;
        private readonly IDomainNotificationHandler<DomainNotification> _notification;
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandHandler"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="bus">The bus.</param>
        /// <param name="notification">The notification.</param>
        public BaseService(IUnitOfWork unitOfWork, IBus bus, IDomainNotificationHandler<DomainNotification> notification)
        {
            _unitOfWork = unitOfWork;
            _notification = notification;
            _bus = bus;
        }

        /// <summary>
        /// Notifies the validation errors.
        /// </summary>
        /// <param name="message">The message.</param>
        protected void NotifyValidationErrors(BaseViewModel viewModel)
        {
            foreach (var error in viewModel.ValidationResult.Errors)
            {
                _bus.RaiseEvent(new DomainNotification(viewModel.GetType().ToString(), error.ErrorMessage));
            }
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <returns></returns>
        public bool Commit()
        {
            if (_notification.HasNotifications())
            {
                return false;
            }

            var commandResponse = _unitOfWork.Commit();

            if (commandResponse.Success)
            {
                return true;
            }

            _bus.RaiseEvent(new DomainNotification("Commit", "We had a problem during saving your data."));

            return false;
        }
    }
}
