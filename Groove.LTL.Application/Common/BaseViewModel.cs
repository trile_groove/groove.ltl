﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseViewModel.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Tracking ViewModel Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Application.Common
{
    using System;
    using FluentValidation.Results;

    /// <summary>
    /// Base ViewModel Class
    /// </summary>
    public abstract class BaseViewModel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// Gets or sets the row version.
        /// </summary>
        public string RowVersion { get; set; }

        /// <summary>
        /// Gets or sets the created user.
        /// </summary>
        public string CreatedUser { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the updated user.
        /// </summary>
        public string UpdatedUser { get; set; }

        /// <summary>
        /// Gets or sets the updated date.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }
        
        /// <summary>
        /// Returns true if the command is valid.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool IsValid();

        public ValidationResult ValidationResult { get; set; }
    }
}
