﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ViewModelToDomainProfile.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   ViewModel to Domain Profile Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Groove.LTL.Domain.Customer.Models;

namespace Groove.LTL.Application.AutoMapper
{
    using global::AutoMapper;
    using Groove.LTL.Application.Customer.ViewModels;

    /// <summary>
    /// ViewModel to Domain Profile Class
    /// </summary>
    public class ViewModelToDomainProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModelToDomainProfile"/> class.
        /// </summary>
        public ViewModelToDomainProfile()
        {
            this.CreateMap<CustomerViewModel, CustomerModel>();
        }
    }
}
