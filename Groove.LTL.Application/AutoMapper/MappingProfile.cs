// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MappingProfile.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Mapping Profile Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Application.AutoMapper
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using global::AutoMapper;

    using Groove.LTL.Application.Common;
    using Groove.LTL.Application.Utilities;
    using Groove.LTL.Domain.Core.Models;
    using Groove.LTL.Domain.Customer.Models;

    /// <summary>
    /// Mapping Profile Class
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MappingProfile"/> class.
        /// </summary>
        public MappingProfile()
        {
            var types = typeof(ICreateMapping).GetTypeInfo().Assembly.GetTypes();

            this.LoadEntityMappings(types);
        }

        /// <summary>
        /// Loads entity mappings.
        /// </summary>
        /// <param name="types">The types.</param>
        private void LoadEntityMappings(IEnumerable<Type> types)
        {
            // Define mapping for base class
            this.CreateMap<BaseViewModel, BaseEntity>().ForMember(x => x.RowVersion, c => c.MapFrom(dto => ByteArrayConverter.FromString(dto.RowVersion)));
            this.CreateMap<BaseEntity, BaseViewModel>().ForMember(x => x.RowVersion, c => c.MapFrom(entity => ByteArrayConverter.ToString(entity.RowVersion)));

            var maps = (from t in types
                        where typeof(ICreateMapping).IsAssignableFrom(t)
                              && !t.GetTypeInfo().IsAbstract
                              && !t.GetTypeInfo().IsInterface
                        select (ICreateMapping)Activator.CreateInstance(t)).ToArray();

            foreach (var map in maps)
            {
                map.CreateMapping(this);
            }
        }
    }
}