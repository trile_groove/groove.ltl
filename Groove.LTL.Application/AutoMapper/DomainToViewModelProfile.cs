﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DomainToViewModelProfile.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Domain to ViewModel Profile Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Application.AutoMapper
{
    using global::AutoMapper;
    using Groove.LTL.Application.Customer.ViewModels;
    using Groove.LTL.Domain.Customer.Models;

    /// <summary>
    /// Domain to ViewModel Profile Class.
    /// </summary>
    public class DomainToViewModelProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DomainToViewModelProfile"/> class.
        /// </summary>
        public DomainToViewModelProfile()
        {
            this.CreateMap<CustomerModel, CustomerViewModel>();
        }
    }
}
