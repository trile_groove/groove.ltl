﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICreateMapping.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Create Mapping Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Application.AutoMapper
{
    using global::AutoMapper;

    /// <summary>
    /// Create Mapping Interface
    /// </summary>
    public interface ICreateMapping
    {
        /// <summary>
        /// Defines the mapping and adds it into the mapping profile
        /// </summary>
        /// <param name="profile">
        /// The profile.
        /// </param>
        void CreateMapping(Profile profile);
    }
}
