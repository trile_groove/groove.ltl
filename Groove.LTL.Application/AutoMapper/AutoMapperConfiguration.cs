﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoMapperConfiguration.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Auto Mapper Configuration Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Application.AutoMapper
{
    using global::AutoMapper;

    /// <summary>
    /// Auto Mapper Configuration Class
    /// </summary>
    public class AutoMapperConfiguration
    {
        /// <summary>
        /// Registers mappings.
        /// </summary>
        /// <returns>
        /// The <see cref="MapperConfiguration"/>.
        /// </returns>
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainToViewModelProfile());
                cfg.AddProfile(new ViewModelToDomainProfile());
                cfg.AddProfile(new MappingProfile());
            });
        }
    }
}
