﻿using System;
using System.Collections.Generic;

using AutoMapper;
using Groove.LTL.Application.Common;
using Groove.LTL.Application.Customer.Interfaces;
using Groove.LTL.Application.Customer.ViewModels;
using Groove.LTL.Domain.Core.Bus;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Notification;
using Groove.LTL.Domain.Customer.Events;
using Groove.LTL.Domain.Customer.Models;
using Groove.LTL.Domain.Customer.Repositories;

namespace Groove.LTL.Application.Customer.Services
{
    /// <summary>
    /// Customer service class
    /// </summary>
    /// <seealso cref="Groove.LTL.Application.Customer.Interfaces.ICustomerService" />
    public class CustomerService : BaseService, ICustomerService
    {
        #region Private Properties
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepository;
        private readonly IBus _bus;
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerService"/> class.
        /// </summary>
        /// <param name="mapper">The mapper.</param>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="bus">The bus.</param>
        public CustomerService(IMapper mapper, ICustomerRepository customerRepository, IUnitOfWork unitOfWork, IBus bus, IDomainNotificationHandler<DomainNotification> notification) : base(unitOfWork, bus, notification) 
        {
            _mapper = mapper;
            _customerRepository = customerRepository;
            _bus = bus;
        }

        /// <summary>
        /// Gets all customer view models.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CustomerViewModel> GetAll()
        {
            return _mapper.Map<IEnumerable<CustomerViewModel>>(_customerRepository.GetAll());
        }

        /// <summary>
        /// Gets the customer view model by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CustomerViewModel GetById(int id)
        {
            return _mapper.Map<CustomerViewModel>(_customerRepository.GetById(id));
        }

        /// <summary>
        /// Creates the specified customer view model.
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        public void Create(CustomerViewModel customerViewModel)
        {
            if (!customerViewModel.IsValid())
            {
                NotifyValidationErrors(customerViewModel);
                return;
            }

            var customer = _mapper.Map<CustomerModel>(customerViewModel);

            _customerRepository.Add(customer);

            if (Commit())
            {
                _bus.RaiseEvent(new CustomerCreatedEvent(customer.Id, customer.CompanyName, customer.ContactName, customer.Email));
            }
        }

        /// <summary>
        /// Updates the specified customer view model.
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        public void Update(CustomerViewModel customerViewModel)
        {
            if (!customerViewModel.IsValid())
            {
                NotifyValidationErrors(customerViewModel);
                return;
            }

            var customer = _mapper.Map<CustomerModel>(customerViewModel);

            _customerRepository.Update(customer);

            if (Commit())
            {
                _bus.RaiseEvent(new CustomerUpdatedEvent(customer.Id, customer.CompanyName, customer.ContactName, customer.Email));
            }
        }

        /// <summary>
        /// Deletes customer by the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(int id)
        {
            _customerRepository.Remove(id);

            if (Commit())
            {
                _bus.RaiseEvent(new CustomerDeletedEvent(id));
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
