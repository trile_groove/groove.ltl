﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICustomerService.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Customer Service Interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Application.Customer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Groove.LTL.Application.Customer.ViewModels;

    /// <summary>
    /// Customer Service Interface
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface ICustomerService : IDisposable
    {
        /// <summary>
        /// Gets all customer view models.
        /// </summary>
        /// <returns>The <see cref="IEnumerable"/>.</returns>
        IEnumerable<CustomerViewModel> GetAll();

        /// <summary>
        /// Gets the customer view model by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The <see cref="CustomerViewModel"/>.</returns>
        CustomerViewModel GetById(int id);

        /// <summary>
        /// Creates the specified customer view model.
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        void Create(CustomerViewModel customerViewModel);

        /// <summary>
        /// Updates the specified customer view model.
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        void Update(CustomerViewModel customerViewModel);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(int id);
    }
}
