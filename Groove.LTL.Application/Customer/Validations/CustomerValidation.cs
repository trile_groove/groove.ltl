﻿using FluentValidation;
using Groove.LTL.Application.Common;
using Groove.LTL.Application.Customer.ViewModels;

namespace Groove.LTL.Application.Customer.Validations
{
    /// <summary>
    /// Customer Validation
    /// </summary>
    public class CustomerValidation : BaseValidation<CustomerViewModel>
    {
        public CustomerValidation()
        {
            RuleFor(c => c.Id)
                .NotEqual(0);
            RuleFor(c => c.CompanyName)
                .NotEmpty().WithMessage("Please ensure you have entered the CompanyName")
                .Length(2, 150).WithMessage("The CompanyName must have between 2 and 150 characters");
            RuleFor(c => c.ContactName)
                .NotEmpty().WithMessage("Please ensure you have entered the ContactName")
                .Length(2, 150).WithMessage("The ContactName must have between 2 and 150 characters");
            RuleFor(c => c.Email)
                .NotEmpty().WithMessage("Please ensure you have entered the Email");
        }
    }
}
