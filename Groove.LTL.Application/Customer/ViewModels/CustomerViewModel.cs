﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerViewModel.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Customer View Model Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Groove.LTL.Application.Customer.Validations;

namespace Groove.LTL.Application.Customer.ViewModels
{
    using Groove.LTL.Application.Common;

    /// <summary>
    /// Customer View Model Class
    /// </summary>
    public class CustomerViewModel : BaseViewModel
    {
        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the contact name.
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        public override bool IsValid()
        {
            ValidationResult = new CustomerValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }
}
