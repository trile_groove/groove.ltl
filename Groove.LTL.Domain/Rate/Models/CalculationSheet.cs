﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalculationSheet.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Calculation Sheet Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Rate.Models
{
    using Groove.LTL.Domain.Core.Models;
    using Groove.LTL.Domain.Customer.Models;

    /// <summary>
    /// Calculation Sheet Class
    /// </summary>
    public class CalculationSheet : BaseEntity
    {
        /// <summary>
        /// Gets or sets the calculation sheet id.
        /// </summary>
        public string CalculationSheetId { get; set; }
    }
}
