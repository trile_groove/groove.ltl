﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerDeletedEvent.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Customer Deleted Event Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Customer.Events
{
    /// <summary>
    /// Customer Deleted Event Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Events.CustomerEvent" />
    public class CustomerDeletedEvent : CustomerEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerDeletedEvent"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public CustomerDeletedEvent(int id)
        {
            this.Id = id;
            this.AggregateId = id;
        }
    }
}
