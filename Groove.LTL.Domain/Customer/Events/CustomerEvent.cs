﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEvent.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Customer Event Class Base
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Customer.Events
{
    using Groove.LTL.Domain.Core.Events;

    /// <summary>
    /// Customer Event Class Base 
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Events.Event" />
    public abstract class CustomerEvent : Event
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the contact name.
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }
    }
}
