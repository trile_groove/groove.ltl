﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEventHandler.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.  
// </copyright>
// <summary>
//   Customer Event Handler Class
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Customer.Events.Handlers
{
    using Groove.LTL.Domain.Core.Interfaces;

    /// <summary>
    /// Customer Event Handler Class
    /// </summary>
    public class CustomerEventHandler : IMessageHandler<CustomerCreatedEvent>, IMessageHandler<CustomerUpdatedEvent>, IMessageHandler<CustomerDeletedEvent>
    {
        /// <summary>
        /// Handles customer created event message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(CustomerCreatedEvent message)
        {
        }

        /// <summary>
        /// Handles customer updated  event message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(CustomerUpdatedEvent message)
        {
        }

        /// <summary>
        /// Handles customer deleted event message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(CustomerDeletedEvent message)
        {
        }
    }
}
