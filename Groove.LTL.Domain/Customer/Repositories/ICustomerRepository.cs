﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICustomerRepository.cs" company="Groove Technology">
//   Copyright (c) Groove Technology. All rights reserved.
// </copyright>
// <summary>
//   Customer Repository Interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Groove.LTL.Domain.Customer.Repositories
{
    using Groove.LTL.Domain.Core.Interfaces;
    using Groove.LTL.Domain.Customer.Models;

    /// <summary>
    /// Customer Repository Interface
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Interfaces.IRepository{CustomerModel}" />
    public interface ICustomerRepository : IRepository<CustomerModel>
    {
    }
}
