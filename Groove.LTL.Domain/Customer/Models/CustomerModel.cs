﻿using System;

using Groove.LTL.Domain.Core.Models;

namespace Groove.LTL.Domain.Customer.Models
{
    /// <summary>
    /// Customer Model Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Models.Entity" />
    public class CustomerModel : BaseEntity
    {
        #region Public Properties
        public string CompanyName { get; set; }

        public string ContactName { get; set; }

        public string Email { get; set; }
        #endregion

        public CustomerModel() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerModel"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="contactName">Name of the contact.</param>
        /// <param name="email">The email.</param>
        public CustomerModel(string companyName, string contactName, string email)
        {
            CompanyName = companyName;
            ContactName = contactName;
            Email = email;
        }
    }
}
